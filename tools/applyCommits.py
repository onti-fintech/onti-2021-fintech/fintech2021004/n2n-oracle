#!/usr/bin/env python
from sys import argv
from dotenv import load_dotenv

from settings import OracleConfig
from utils.client import Client
from utils.contract import DeployedContract

load_dotenv()
config = OracleConfig()
id = argv[1]

right_client = Client(
    config.right_url, config.private_key, config.left_gas_price)
left_client = Client(
    config.left_url, config.private_key, config.left_gas_price)

right_contract = DeployedContract(
    right_client,
    address=config.right_address,
    abi=config.right_bridge_build["abi"],
)
left_contract = DeployedContract(
    left_client,
    address=config.left_address,
    abi=config.left_bridge_build["abi"],
)

r_arguments = []
s_arguments = []
v_arguments = []

nth = 0
while True:
    r, s, v = right_contract.getCommit(id, nth).call()
    if r == s == v == 0:
        break

    r_arguments.append(r)
    s_arguments.append(s)
    v_arguments.append(v)

    nth += 1

recipient, amount = right_contract.getTransferDetails(id).call()

receipt = left_contract.applyCommits(
    recipient, amount, id,
    r_arguments, s_arguments, v_arguments,
).transact()

print(receipt["transactionHash"].hex(), "executed")
