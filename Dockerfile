FROM python:latest

WORKDIR /

COPY . .

ENV PYTHONUNBUFFERED=1
ENV DATABASE_PATH=/data/state.db
ENV BUILD_PATH=/contracts/build/contracts/

RUN pip install -r deployment/requirements.txt
RUN pip install -r oracle/requirements.txt

RUN chmod +x /tools/applyCommits.py

CMD python /oracle/main.py
#docker run -ti --rm --env-file .env n2n-oracle /tools/applyCommits.py 0xac21bd9c034f02266f79ac064560ea1ed3e8b8ff6491f0f8e433e9122ae4e804
