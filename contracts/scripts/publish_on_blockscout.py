import requests
from eth_abi import encode_abi
from web3 import Web3

BASE_URL = 'https://blockscout.com/poa/sokol/api'


def encode_constructor_args(args_pattern: list, constructor_args: list):
    return Web3.toHex(
        encode_abi(args_pattern, constructor_args)
    ).removeprefix('0x')


def publish_source(address, silent=False, **kwargs):
    version_prefix = 'v'
    if not kwargs['compiler_version'].startswith(version_prefix):
        kwargs['compiler_version'] = version_prefix + kwargs['compiler_version']

    data = {
        'module': 'contract',
        'action': 'verify',
        'addressHash': address,
        'name': kwargs['contract_name'],
        'compilerVersion': kwargs['compiler_version'],
        'optimizationRuns': kwargs['optimizer_runs'],
        'optimization': kwargs['optimizer_enabled'],
        'contractSourceCode': kwargs['flattened_source'],
    }
    if 'encoded_constructor_arguments' in kwargs:
        data['constructorArguments'] = kwargs['encoded_constructor_arguments']

    response = requests.post(url=BASE_URL, json=data)

    if not silent:
        if response.status_code == 200 and response.json()['status'] == '1':
            print(f'Contract {address} successfully verified.')
        else:
            print(f'An error occurred while publishing the contract source code:', response.content, sep='\n')
