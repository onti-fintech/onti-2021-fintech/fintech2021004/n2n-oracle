"""
Скрипт для тестов.

Генерирует рандомные ивенты и посылает их в блокчейн
"""
import dotenv
import os

from brownie import accounts, LeftBridge, RightBridge, network

NUMBER_OF_EVENTS = 1


def generate_events(contract, sender, n=NUMBER_OF_EVENTS):
    for _ in range(n):
        sender.transfer(contract.address, "1 ether")


def main():
    dotenv_path = os.getcwd() + '/../.env'
    print(dotenv_path)
    values = dotenv.dotenv_values(dotenv_path)
    left = LeftBridge.at(values['LEFT_ADDRESS'])
    right = RightBridge.at(values['RIGHT_ADDRESS'])

    if network.show_active() == 'development':
        sender = accounts[0]
    else:
        private_key = values['PRIVKEY']
        sender = accounts.add(private_key=private_key)

    left.addLiquidity({"from": sender, 'value': "10 ether"})
    right.addLiquidity({"from": sender, 'value': "10 ether"})
    left.enableRobustMode({"from": sender})
    right.enableRobustMode({"from": sender})

    tx = accounts[1].transfer(left.address, "5 ether")
    right.commit(accounts[1].address, 5e18, tx.txid, {"from": sender})

    generate_events(right, accounts[1])
