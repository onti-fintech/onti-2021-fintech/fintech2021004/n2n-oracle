// SPDX-License-Identifier: None

pragma solidity >=0.7.5;

import "./BridgeValidators.sol";
import "./selfDestructContract.sol";

contract NativeTokenBridge {
    address public owner;
    address public validatorSet;
    uint256 liquidityLimit;
    uint256 initLiquidityLimit = 0;
    bool isRight;
    uint256 min = 0;
    uint256 max = 0;
    bool public robustMode = false;
    bool isOperationsStopped = false;

    struct detailedAddressDict {
        bool isConfirmedByAddres;
        uint8 index;
        bytes32 r;
        bytes32 s;
        uint8 v;
    }

    struct commits {
        mapping(address => detailedAddressDict) addresses;
        address recipient;
        uint256 amount;
        bool isConfirmed;
    }

    mapping(bytes32 => commits) public confirmations;

    event bridgeActionInitiated(address recipient, uint256 amount);

    modifier onlyOwner() {
        require(owner == msg.sender, "Only owner can access this method.");
        _;
    }

    modifier isOperationsOnline() {
        require(!isOperationsStopped, "Operations stopped");
        _;
    }

    modifier isValuable(uint256 value) {
        require(value > min, "You should send more than min ether.");
        if (max != 0) {
            require(value < max, "You should send less than max ether.");
        }
        _;
    }

    modifier isInRange(bool addOrRemove) {
        if (addOrRemove) {
            require(liquidityLimit + msg.value <= initLiquidityLimit, "Too many tokens for the bridge.");
        } else {
            require(liquidityLimit >=  msg.value, "Too many tokens for the bridge...");
        }
        _;
    }

    modifier OnlyValidator() {
        BridgeValidators validators = BridgeValidators(validatorSet);
        require(
            validators.isValidator(msg.sender),
            "Only validator can access this method."
        );
        _;
    }

    receive() external payable isValuable(msg.value) isInRange(isRight) isOperationsOnline {
        emit bridgeActionInitiated(msg.sender, msg.value);
        if (isRight) {
            liquidityLimit += msg.value;
        } else {
            liquidityLimit -= msg.value;
        }
    }

    function stopOperations() public {
        isOperationsStopped = true;
    }

    function startOperations() public {
        isOperationsStopped = false;
    }

    function addLiquidity() public payable onlyOwner isValuable(msg.value) {
        initLiquidityLimit +=  msg.value;
        liquidityLimit += msg.value;
    }

    function getLiquidityLimit() public view returns (uint256) {
        return liquidityLimit;
    }

    function changeValidatorSet(address newValidatorSet) public onlyOwner {
        require(validatorSet != newValidatorSet, "The same address.");
        validatorSet = newValidatorSet;
    }

    function countConfirmations(bytes32 actionId)
        public
        view
        returns (uint256)
    {
        BridgeValidators validators = BridgeValidators(validatorSet);
        address[] memory data = validators.getValidators();

        uint256 count = 0;
        for (uint256 i = 0; i < data.length; i++) {
            if (confirmations[actionId].addresses[data[i]].isConfirmedByAddres) {
                count += 1;
            }
        }
        return count;
    }

    function confirmAction(bytes32 actionId) internal {
        require(
            !confirmations[actionId].addresses[msg.sender].isConfirmedByAddres,
            "You've already confirmed this action."
        );
        confirmations[actionId].addresses[msg.sender].isConfirmedByAddres = true;
    }

    // function isConfirmed(bytes32 actionId) public view returns(bool) {
    //     BridgeValidators validators = BridgeValidators(validatorSet);
    //     return countConfirmations(actionId) >= validators.getThreshold();
    // }

    // function deleteAction(bytes32 actionId) internal {
    //     BridgeValidators validators = BridgeValidators(validatorSet);
    //     address[] memory data = validators.getValidators();

    //     for (uint i = 0; i < data.length; i++) {
    //         delete confirmations[actionId][data[i]];
    //     }
    // }


    function commit(
        address payable recipient,
        uint256 amount,
        bytes32 id
    ) public OnlyValidator isValuable(amount) {

        require(!confirmations[id].isConfirmed, "The action was already perfomed.");

        if (!isRight) {
            require(!robustMode);
        }
        BridgeValidators validators = BridgeValidators(validatorSet);
        require(countConfirmations(id) < validators.getThreshold(), "The action was already perfomed.");

        confirmAction(id);

        require(countConfirmations(id) <= validators.getThreshold(), "The action was already perfomed.");

        if (countConfirmations(id) == validators.getThreshold()) {
            selfDestructContract DestructContract = new selfDestructContract(recipient);
            payable(DestructContract).transfer(amount);
            DestructContract.destroy();

            confirmations[id].isConfirmed = true;

            address[] memory data = validators.getValidators();
            for (uint256 i = 0; i < data.length; i++) {
                delete confirmations[id].addresses[data[i]].isConfirmedByAddres;
            }
            if (!isRight) {
                liquidityLimit += amount;
            } else {
                liquidityLimit -= amount;
            }
        }
    }

    function isIdConfirmed(bytes32 id) public view returns (bool) {
        return confirmations[id].isConfirmed;
    }

    function isIdConfirmedByMe(bytes32 id) public view returns (bool) {
        return confirmations[id].addresses[msg.sender].isConfirmedByAddres;
    }

    function enableRobustMode() public onlyOwner {
        robustMode = true;
    }

    function getRobustModeMessage(address recipient, uint256 amount, bytes32 id) public view returns(bytes32) {
        return keccak256(abi.encodePacked(recipient, amount, id));
    }

    function computeEcrecover(address recipient, uint256 amount, bytes32 id, uint256 r, uint256 s, uint8 v) internal returns (address) {
        require(!confirmations[id].isConfirmed, "The action was already perfomed.");
        bytes32 messageHash = getRobustModeMessage(recipient, amount, id);
        bytes32 ethSignedMessageHash = keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", messageHash));
        bytes32 rbytes = bytes32(r);
        bytes32 sbytes = bytes32(s);
        return ecrecover(ethSignedMessageHash, v, rbytes, sbytes);
    }

    function setMinPerTx(uint256 _min) public {
        min = _min;
    }

    function setMaxPerTx(uint256 _max) public {
        max = _max;
    }
}
