// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

import "./NativeTokenBridge.sol";


contract RightBridge is NativeTokenBridge {

    event commitsCollected(bytes32 id, uint8 commits);

    constructor(address _validatorSet) {
        owner = msg.sender;
        validatorSet = _validatorSet;
        isRight = true;
    }

    function confirmRobustAction(bytes32 actionId, address recipient, uint256 amount, bytes32 r, bytes32 s, uint8 v, uint256 countConfirms) internal {
        require(
            !confirmations[actionId].addresses[msg.sender].isConfirmedByAddres,
            "You've already confirmed this action."
        );
        confirmations[actionId].recipient = recipient;
        confirmations[actionId].amount = amount;
        confirmations[actionId].addresses[msg.sender].isConfirmedByAddres = true;
        confirmations[actionId].addresses[msg.sender].index = uint8(countConfirms);
        confirmations[actionId].addresses[msg.sender].r = r;
        confirmations[actionId].addresses[msg.sender].s = s;
        confirmations[actionId].addresses[msg.sender].v = v;
    }

    function registerCommit(address recipient, uint256 amount, bytes32 id, uint256 r, uint256 s, uint8 v) public {
        require(!confirmations[id].isConfirmed, "The action was already perfomed.");
        address signer = msg.sender;
        bool isSigned = (computeEcrecover(recipient, amount, id , r, s, v) == signer);
        require(isSigned, "Wrong Singature right");

        BridgeValidators validators = BridgeValidators(validatorSet);
        uint256 countConfirms = countConfirmations(id);

        confirmRobustAction(id, recipient, amount, bytes32(r), bytes32(s), v, countConfirms);

        if (countConfirms+1 == validators.getThreshold()) {
            emit commitsCollected(id, uint8(validators.getThreshold()));
            confirmations[id].isConfirmed = true;
        }
    }

    function getTransferDetails(bytes32 id) public view returns (address recipient, uint256 amount) {
        return (confirmations[id].recipient, confirmations[id].amount);
    }

    function getCommit(bytes32 id, uint8 index) public view returns (uint256 r, uint256 s, uint8 v) {
        BridgeValidators validators = BridgeValidators(validatorSet);
        address[] memory data = validators.getValidators();
        for (uint256 i = 0; i < data.length; i++) {
            if (confirmations[id].addresses[data[i]].index == index) {
                return (
                    uint256(confirmations[id].addresses[data[i]].r),
                    uint256(confirmations[id].addresses[data[i]].s),
                    confirmations[id].addresses[data[i]].v
                    );
            }
        }
    }

}

