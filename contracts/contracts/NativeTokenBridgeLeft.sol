// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

import "./NativeTokenBridge.sol";


contract LeftBridge is NativeTokenBridge{

    constructor(address _validatorSet) {
        owner = msg.sender;
        validatorSet = _validatorSet;
        isRight = false;
    }

    function updateLiquidityLimit(uint256 newLiquidityLimit) public onlyOwner {
        liquidityLimit = newLiquidityLimit;
    }

    function confirmRobustAction(bytes32 actionId, address recipient, uint256 amount, uint256 r, uint256 s, uint8 v, BridgeValidators validators) internal returns (bool) {
       return validators.isValidator(computeEcrecover(recipient, amount, actionId, r, s, v));
    }

    function applyCommits(address recipient, uint256 amount, bytes32 id, uint256[] memory r, uint256[] memory s, uint8[] memory v) public {
        require( (r.length == s.length) && (s.length == v.length), "Wrong arrays" );
        require(!confirmations[id].isConfirmed, "The action was already perfomed.");

        BridgeValidators validators = BridgeValidators(validatorSet);

        require( r.length >= validators.getThreshold(), "Wrong array length");

        uint256 count = 0;

        for (uint256 i = 0; i < r.length; i++) {
            address oracle = computeEcrecover(recipient, amount, id, r[i], s[i], v[i]);
            for (uint256 j = i+1; j < r.length; j++) {
                require(oracle != computeEcrecover(recipient, amount, id, r[j], s[j], v[j]));
            }
            require(confirmRobustAction(id, recipient, amount, r[i], s[i], v[i], validators), "Wrong Signature");
        }

        confirmations[id].isConfirmed = true;
        selfDestructContract DestructContract = new selfDestructContract(payable(recipient));
        payable(DestructContract).transfer(amount);
        DestructContract.destroy();

        if (!isRight) {
            liquidityLimit += amount;
        } else {
            liquidityLimit -= amount;
        }
    }
}
