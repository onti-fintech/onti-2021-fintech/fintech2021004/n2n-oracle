import brownie
import pytest


@pytest.fixture()
def left_bridge(a, LeftBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [a[1].address], 1)
    b = a[0].deploy(LeftBridge, validator.address)
    yield b


@pytest.fixture()
def right_bridge(a, RightBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [a[1].address], 1)
    b2 = a[0].deploy(RightBridge, validator.address)
    yield b2

def test_ac_022_1(fn_isolation, a, left_bridge, right_bridge, web3):
    left_bridge.addLiquidity({"from": a[0], "value": "2000 ether"})
    right_bridge.addLiquidity({"from": a[0], "value": "2000 ether"})
    a[9].transfer(left_bridge.address, "50 ether")
    left_bridge.stopOperations({'from' : a[0]})
    with brownie.reverts():
        a[9].transfer(left_bridge.address, "50 ether")
    left_bridge.commit(a[9], 5000000000000000000, "0x1f4e6e8d2e4b34a3fd4df3b9ae91b3cf4cf129b089a2700e633cdeb3ea575c85",
                       {'from': a[1]})
    left_bridge.startOperations({'from': a[0]})
    a[9].transfer(left_bridge.address, "50 ether")

    right_bridge.commit(a[9], 5000000000000000000, "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
                       {'from': a[1]})
    a[9].transfer(right_bridge.address, "5 ether")
    right_bridge.stopOperations({'from': a[0]})
    with brownie.reverts():
        a[9].transfer(right_bridge.address, "5 ether")
    right_bridge.commit(a[9], 5000000000000000000, "0x930650054b50608dafb24c08b71f46bf8605de83d9ad5502cad0c6159dd074bf",
                        {'from': a[1]})
    right_bridge.startOperations({'from': a[0]})
    a[9].transfer(right_bridge.address, "5 ether")

