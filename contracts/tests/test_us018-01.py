import brownie
import pytest
from brownie import web3, accounts
from web3 import Web3
from eth_account.messages import encode_defunct
from eth_account import Account
from brownie import accounts

PRIVATE_KEY = '5192d4ae0860e5c4d5e02c824a8d175c66831f7c58a9396072f53d29c1d94a8c'

PRIVATE_KEY2 = '6192d4ae0860e5c4d5e02c824a8d175c66831f7c58a9396072f53d29c1d94a8c'

PRIVATE_KEY3 = '7192d4ae0860e5c4d5e02c824a8d175c66831f7c58a9396072f53d29c1d94a8c'


@pytest.fixture()
def oracle(a):
    o = accounts.add(private_key=PRIVATE_KEY)
    a[0].transfer(o, '1 ether')
    return o


@pytest.fixture()
def oracle2(a):
    o = accounts.add(private_key=PRIVATE_KEY2)
    a[0].transfer(o, '1 ether')
    return o


@pytest.fixture()
def oracle3(a):
    o = accounts.add(private_key=PRIVATE_KEY3)
    a[0].transfer(o, '1 ether')
    return o


@pytest.fixture()
def left_bridge(a, oracle, oracle2, oracle3, LeftBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [oracle, oracle2, oracle3], 2)
    b = a[0].deploy(LeftBridge, validator.address)
    yield b


@pytest.fixture()
def right_bridge(a, oracle, oracle2, oracle3, RightBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [oracle, oracle2, oracle3], 2)
    b2 = a[0].deploy(RightBridge, validator.address)
    yield b2


# def test_get_robust_msg(a, right_bridge):
#     recipient = '0xF91296B9d7699d800c2Ba0a80e755972a9DA7C34'
#     amount = 10000000000000000000
#     _id = '0xac21bd9c034f02266f79ac064560ea1ed3e8b8ff6491f0f8e433e9122ae4e804'
#     msg_bytes = right_bridge.getRobustModeMessage(
#         recipient,
#         amount,
#         _id
#     )
#     message = web3.toBytes(hexstr=recipient) + amount.to_bytes(32, "big") + web3.toBytes(hexstr=_id)
#     message_hash = web3.keccak(message)
#     local_bytes = encode_defunct(hexstr=message_hash.hex())
#     print(local_bytes)
#     message_hash = web3.keccak(message)
#
#     assert msg_bytes == local_bytes.body


def test_ac_018_01(fn_isolation, oracle, oracle2, oracle3, a, left_bridge, right_bridge, BridgeValidators):
    left_bridge.addLiquidity({"from": a[0], "value": "2000 ether"})
    right_bridge.addLiquidity({"from": a[0], "value": "2000 ether"})
    a[9].transfer(left_bridge.address, "1000 ether")
    left_bridge.enableRobustMode()
    right_bridge.enableRobustMode()

    print('test')

    recipient = '0xF91296B9d7699d800c2Ba0a80e755972a9DA7C34'
    amount = 10000000000000000000
    _id = '0xac21bd9c034f02266f79ac064560ea1ed3e8b8ff6491f0f8e433e9122ae4e804'
    msg = right_bridge.getRobustModeMessage(
        recipient,
        amount,
        _id
    )
    # message = web3.toBytes(hexstr=recipient) + amount.to_bytes(32, "big") + Web3.toBytes(hexstr=_id)
    # print(web3.toBytes(msg))
    # print(message)

    # assert web3.toBytes(msg) == web3.keccak(message)

    # print(msg)
    # print(type(msg))
    # print(web3.toBytes(msg))
    sign_able = encode_defunct(web3.toBytes(msg))

    signed_message = Account.sign_message(
        sign_able,
        private_key=PRIVATE_KEY
    )
    signed_message2 = Account.sign_message(
        sign_able,
        private_key=PRIVATE_KEY2
    )

    signed_message3 = Account.sign_message(
        sign_able,
        private_key=PRIVATE_KEY3
    )
    # print(signed_message.r)
    # print(signed_message.s)
    # print(signed_message.v)
    tx = right_bridge.registerCommit(
        recipient,
        amount,
        _id,
        signed_message.r,
        signed_message.s,
        signed_message.v,
        {'from': oracle}
    )
    tx = right_bridge.registerCommit(
        recipient,
        amount,
        _id,
        signed_message2.r,
        signed_message2.s,
        signed_message2.v,
        {'from': oracle2}
    )
    print(tx.events['commitsCollected'])
    assert tx

    with brownie.reverts():
        tx = right_bridge.registerCommit(
            recipient,
            amount,
            _id,
            signed_message3.r,
            signed_message3.s,
            signed_message3.v,
            {'from': oracle3}
        )

    tx = right_bridge.getTransferDetails(_id)
    print(tx)
    rsv1 = right_bridge.getCommit(_id, 0)
    rsv2 = right_bridge.getCommit(_id, 1)

    print([rsv1[0], rsv2[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]])



    left_bridge.applyCommits(recipient, amount, _id,
       [rsv1[0], rsv2[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]], {'from': oracle}
    )


    # print(oracle.address)
    # print(tx)
    # print(tx.events['commitsCollected'])
    with brownie.reverts():
        left_bridge.commit(
            a[8].address, 1000e18,
            "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
            {"from": oracle}
        )
    # with brownie.reverts():
    #     left_bridge.applyCommits(recipient, amount, _id,
    #                              [rsv1[0]], [rsv1[1]], [rsv1[2]], {'from': oracle}
    #                              )

    # with brownie.reverts():
    #     left_bridge.applyCommits(recipient, amount+1000000, _id,
    #        [rsv1[0], rsv2[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]], {'from': oracle}
    #     )
    #
    # with brownie.reverts():
    #     left_bridge.applyCommits(recipient, amount, _id,
    #        [rsv1[0], rsv1[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]], {'from': oracle}
    #     )
    #
    # with brownie.reverts():
    #     left_bridge.applyCommits(recipient, amount, _id,
    #        [rsv1[0], rsv1[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]], {'from': oracle}
    #     )
    with brownie.reverts():
        left_bridge.applyCommits(recipient, amount, _id,
           [rsv1[0], rsv1[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]], {'from': oracle}
        )

    with brownie.reverts():
        left_bridge.applyCommits(recipient, amount, _id,
            [rsv1[0], rsv2[0]], [rsv1[1], rsv2[1]], [rsv1[2], rsv2[2]], {'from': oracle}
        )

    with brownie.reverts():
        left_bridge.applyCommits(recipient, amount, _id,
            [rsv1[0], rsv1[0]], [rsv1[1], rsv1[1]], [rsv1[2], rsv1[2]], {'from': oracle}
        )