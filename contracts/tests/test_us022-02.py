import brownie
import pytest


@pytest.fixture()
def left_bridge(a, LeftBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [a[1].address, a[2].address, a[3].address], 1)
    b = a[0].deploy(LeftBridge, validator.address)
    yield b


@pytest.fixture()
def right_bridge(a, RightBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [a[1].address, a[2].address, a[3].address], 1)
    b2 = a[0].deploy(RightBridge, validator.address)
    yield b2


def test_ac_022_2(fn_isolation, a, left_bridge, right_bridge, web3):
    right_bridge.addLiquidity({"from": a[0], "value": "2000 ether"})
    left_bridge.addLiquidity({"from": a[0], "value": "2000 ether"})
    a[8].transfer(left_bridge.address, "1000 ether")
    right_bridge.commit(
        a[8].address, 1000e18,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    assert a[8].balance() == "5000 ether"
    left_bridge.commit(
        a[9].address, 100000000000000000000,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[2]}
    )
    a[9].transfer(left_bridge.address, "100 ether")
    left_bridge.setMaxPerTx(99999999999999999999, {"from": a[0]})
    with brownie.reverts():
        left_bridge.commit(
            a[9].address, 100000000000000000000,
            "0x930650054b50608dafb24c08b71f46bf8605de83d9ad5502cad0c6159dd074bf",
            {"from": a[2]}
        )
    left_bridge.commit(
        a[9].address, 99000000000000000000,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb286",
        {"from": a[2]}
    )
