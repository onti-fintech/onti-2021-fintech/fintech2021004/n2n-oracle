import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address, a[2].address, a[3].address], 2)


@pytest.fixture()
def bridge(accounts, validator, LeftBridge):
    b = accounts[0].deploy(LeftBridge, validator.address)
    yield b


def test_ac_009_01(fn_isolation, a, bridge, web3):
    bridge.updateLiquidityLimit(200000000000000000000, {"from": a[0]})
    assert bridge.getLiquidityLimit() == "200 ether"

    receipt = a[4].transfer(bridge.address, amount="100 ether")
    assert bridge.balance() == "100 ether"
    # todo: check for event

    bridge.commit(
        a[8].address, 50000000000000000000,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    assert bridge.balance() == "100 ether"

    with brownie.reverts():
        bridge.commit(
            a[8].address, 50000000000000000000,
            "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
            {"from": a[1]}
        )

    bridge.commit(
        a[9].address, 15000000000000000000,
        "0xeb096ea9048b5287eba951adda5b20a8eae9bf9dccc8ff34a44f984b0ed44fd2",
        {"from": a[1]}
    )
    assert bridge.balance() == "100 ether"

    user_balance_before = a[8].balance()
    bridge.commit(
        a[8].address, 50000000000000000000,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[3]}
    )
    user_balance_after = a[8].balance()
    assert user_balance_after - user_balance_before == "50 ether"
    assert bridge.balance() == "50 ether"

    user_balance_before = a[9].balance()
    bridge.commit(
        a[9].address, 15000000000000000000,
        "0xeb096ea9048b5287eba951adda5b20a8eae9bf9dccc8ff34a44f984b0ed44fd2",
        {"from": a[2]}
    )
    user_balance_after = a[9].balance()
    assert user_balance_after - user_balance_before == "15 ether"
    assert bridge.balance() == "35 ether"

    with brownie.reverts():
        bridge.commit(
            a[9].address, 15000000000000000000,
            "0xeb096ea9048b5287eba951adda5b20a8eae9bf9dccc8ff34a44f984b0ed44fd2",
            {"from": a[3]}
        )
