import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address], 1)


@pytest.fixture()
def bridge(accounts, validator, RightBridge):
    b = accounts[0].deploy(RightBridge, validator.address)
    yield b


def test_ac_010_01(fn_isolation, a, bridge, web3):
    with brownie.reverts():
        a[9].transfer(bridge.address, amount="5 ether")

    a[7].transfer(a[0], '100 ether')
    a[6].transfer(a[0], '100 ether')
    bridge.addLiquidity({"from": a[0], "value": "200 ether"})
    assert bridge.getLiquidityLimit() == "200 ether"

    with brownie.reverts():
        a[9].transfer(bridge.address, amount="5 ether")

    user_balance_before = a[8].balance()
    bridge.commit(
        a[8].address, 100e18,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    user_balance_after = a[8].balance()
    assert user_balance_after - user_balance_before == "100 ether"
    assert bridge.balance() == "100 ether"

    assert bridge.getLiquidityLimit() == "100 ether"

    receipt = a[9].transfer(bridge.address, amount="5 ether")
    # todo: check for event

    assert bridge.balance() == "105 ether"
    assert bridge.getLiquidityLimit() == "105 ether"

    receipt = a[8].transfer(bridge.address, amount="10 ether")
    # todo: check for event

    assert bridge.balance() == "115 ether"
    assert bridge.getLiquidityLimit() == "115 ether"

    receipt = a[8].transfer(bridge.address, amount="15 ether")
    # todo: check for event

    assert bridge.balance() == "130 ether"
    assert bridge.getLiquidityLimit() == "130 ether"

    with brownie.reverts():
        a[8].transfer(bridge.address, amount="75 ether")

    user_balance_before = a[9].balance()
    bridge.commit(
        a[9].address, 50e18,
        "0xc05af7a8256ab341d0f51a464e9d84dbde0efeb1f36b48ae815f179410e7a04e",
        {"from": a[1]}
    )
    user_balance_after = a[9].balance()
    assert user_balance_after - user_balance_before == "50 ether"
    assert bridge.balance() == "80 ether"

    assert bridge.getLiquidityLimit() == "80 ether"

    receipt = a[8].transfer(bridge.address, amount="75 ether")
    assert bridge.balance() == "155 ether"

    assert bridge.getLiquidityLimit() == "155 ether"

    with brownie.reverts():
        a[8].transfer(bridge.address, amount="0 ether")
