import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address], 1)


@pytest.fixture()
def bridge(accounts, validator, RightBridge):
    b = accounts[0].deploy(RightBridge, validator.address)
    yield b


@pytest.fixture()
def contractfamilyWallet(accounts, familyWallet):
    f = accounts[0].deploy(familyWallet, accounts[2], accounts[3])
    yield f


@pytest.fixture()
def contractfamilyWallet2(accounts, familyWallet):
    f2 = accounts[0].deploy(familyWallet, accounts[4], accounts[5])
    yield f2


def test_ac_017(fn_isolation, a, bridge, contractfamilyWallet, contractfamilyWallet2, web3):
    bridge.addLiquidity({"from": a[0], "value": "100 ether"})

    bridge.commit(
        contractfamilyWallet.address, 50000000000000000000,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    assert contractfamilyWallet.balance() == "50 ether"

    bridge.commit(
        contractfamilyWallet2.address, 5000000000000000000,
        "0xbe4da129fc927472b7b5ab00c04a0c19124fd3915a51650e672f332dcc8d2f3d",
        {"from": a[1]}
    )
    assert contractfamilyWallet2.balance() == "5 ether"
    assert bridge.balance() == "45 ether"
