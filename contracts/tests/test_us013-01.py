import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address], 1)


@pytest.fixture()
def left_bridge(accounts, validator, LeftBridge):
    b = accounts[0].deploy(LeftBridge, validator.address)
    yield b


@pytest.fixture()
def right_bridge(accounts, validator, RightBridge):
    b2 = accounts[0].deploy(RightBridge, validator.address)
    yield b2


def test_ac_010_01(fn_isolation, a, left_bridge, right_bridge, web3):
    left_bridge.addLiquidity({"from": a[0], "value": "100 ether"})
    a[6].transfer(a[0], '100 ether')
    right_bridge.addLiquidity({"from": a[0], "value": "100 ether"})
    a[2].transfer(left_bridge.address, amount="100 ether")
    right_bridge.commit(
        a[2].address, 100e18,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    a[3].transfer(right_bridge.address, amount="10 ether")
    left_bridge.commit(
        a[3].address, 10e18,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
