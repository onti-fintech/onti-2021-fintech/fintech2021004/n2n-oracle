import brownie
import pytest


@pytest.fixture()
def left_bridge(a, LeftBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [a[1].address], 1)
    b = a[0].deploy(LeftBridge, validator.address)
    yield b


@pytest.fixture()
def right_bridge(a, RightBridge, BridgeValidators):
    validator = a[0].deploy(BridgeValidators, [a[1].address], 1)
    b2 = a[0].deploy(RightBridge, validator.address)
    yield b2


def test_ac_016_03(fn_isolation, a, left_bridge, right_bridge, BridgeValidators):
    # liquidity
    left_bridge.addLiquidity({"from": a[0], "value": "20 ether"})
    right_bridge.addLiquidity({"from": a[0], "value": "20 ether"})

    # 1
    balance_before = a[9].balance()
    a[9].transfer(left_bridge.address, "10 ether")
    balance_after = a[9].balance()
    assert balance_before - balance_after == "10 ether"
    assert left_bridge.balance() == "30 ether"

    balance_before = a[9].balance()
    right_bridge.commit(a[9].address, 10e18, f"0x{1:0>64x}", {"from": a[1]})
    balance_after = a[9].balance()
    assert balance_after - balance_before == "10 ether"
    assert right_bridge.balance() == "10 ether"

    # 2
    balance_before = a[9].balance()
    a[9].transfer(right_bridge.address, "0.1 ether")
    balance_after = a[9].balance()
    assert balance_before - balance_after == "0.1 ether"
    assert right_bridge.balance() == "10.1 ether"

    balance_before = a[9].balance()
    left_bridge.commit(a[9].address, 0.1e18, f"0x{2:0>64x}", {"from": a[1]})
    balance_after = a[9].balance()
    assert balance_after - balance_before == "0.1 ether"
    assert left_bridge.balance() == "29.9 ether"

    # 3 - stop oracle

    # 4
    balance_before = a[8].balance()
    a[8].transfer(right_bridge.address, "0.27 ether")
    balance_after = a[8].balance()
    assert balance_before - balance_after == "0.27 ether"
    assert right_bridge.balance() == "10.37 ether"

    # 5
    balance_before = a[7].balance()
    a[7].transfer(left_bridge.address, "0.15 ether")
    balance_after = a[7].balance()
    assert balance_before - balance_after == "0.15 ether"
    assert left_bridge.balance() == "30.05 ether"

    # 6
    left_validators = BridgeValidators.at(left_bridge.validatorSet())
    right_validators = BridgeValidators.at(right_bridge.validatorSet())
    left_validators.addValidator(a[2].address, {"from": a[0]})
    right_validators.addValidator(a[2].address, {"from": a[0]})

    # 7
    left_validators.removeValidator(a[1].address, {"from": a[0]})
    right_validators.removeValidator(a[1].address, {"from": a[0]})

    # 8 - restart oracle
    with brownie.reverts():
        right_bridge.commit(a[9].address, 10e18,
                            f"0x{1:0>64x}", {"from": a[2]})
    with brownie.reverts():
        left_bridge.commit(a[9].address, 0.1e18,
                           f"0x{2:0>64x}", {"from": a[2]})

    # 9
    balance_before = a[8].balance()
    left_bridge.commit(a[8].address, 0.27e18, f"0x{3:0>64x}", {"from": a[2]})
    balance_after = a[8].balance()
    assert balance_after - balance_before == "0.27 ether"
    assert left_bridge.balance() == "29.78 ether"

    balance_before = a[7].balance()
    right_bridge.commit(a[7].address, 0.15e18, f"0x{4:0>64x}", {"from": a[2]})
    balance_after = a[7].balance()
    assert balance_after - balance_before == "0.15 ether"
    assert right_bridge.balance() == "10.22 ether"
