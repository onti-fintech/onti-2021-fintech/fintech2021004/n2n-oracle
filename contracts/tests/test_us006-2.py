import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address], 1)


@pytest.fixture()
def bridge(accounts, validator, RightBridge):
    b = accounts[0].deploy(RightBridge, validator.address)
    yield b


def test_ac_009_01(fn_isolation, a, bridge, web3):
    bridge.addLiquidity({"from": a[0], "value": "100 ether"})
    assert bridge.getLiquidityLimit() == "100 ether"

    a[6].transfer(a[0], '50 ether')

    bridge.addLiquidity({"from": a[0], "value": "50 ether"})
    assert bridge.getLiquidityLimit() == "150 ether"