import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address], 1)


@pytest.fixture()
def bridge(accounts, validator, RightBridge):
    b = accounts[0].deploy(RightBridge, validator.address)
    yield b


def test_ac_009_01(fn_isolation, a, bridge, web3):
    with brownie.reverts():
        bridge.commit(
            a[8].address, 50000000000000000000,
            "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
            {"from": a[1]}
        )
    bridge.addLiquidity({"from": a[0], "value": "100 ether"})
    assert bridge.getLiquidityLimit() == "100 ether"

    user_balance_before = a[8].balance()
    bridge.commit(
        a[8].address, 50000000000000000000,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    user_balance_after = a[8].balance()
    assert user_balance_after - user_balance_before == "50 ether"

    assert bridge.balance() == "50 ether"
    assert bridge.getLiquidityLimit() == "50 ether"

    with brownie.reverts():
        bridge.commit(
            a[8].address, 51000000000000000000,
            "0xbe4da129fc927472b7b5ab00c04a0c19124fd3915a51650e672f332dcc8d2f3d",
            {"from": a[1]}
        )

    bridge.commit(
        a[9].address, 5000000000000000000,
        "0xbe4da129fc927472b7b5ab00c04a0c19124fd3915a51650e672f332dcc8d2f3d",
        {"from": a[1]}
    )

    assert bridge.balance() == "45 ether"
    assert bridge.getLiquidityLimit() == "45 ether"

    with brownie.reverts():
        bridge.commit(
            a[4].address, 5000000000000000000,
            "0x5840f306777b4982348202e3ff40c17e6ae555b228d5f0290aa23c23d36423d1",
            {"from": a[0]}
        )

    user_balance_before = a[9].balance()
    bridge.commit(
        a[9].address, 5000000000000000000,
        "0x5d46b2de62ae6807ad8a2cf1342b2b7bf547328f23a5439511297798b9cadfbc",
        {"from": a[1]}
    )
    user_balance_after = a[9].balance()

    assert user_balance_after - user_balance_before == "5 ether"

    assert bridge.balance() == "40 ether"
    assert bridge.getLiquidityLimit() == "40 ether"

    with brownie.reverts():
        bridge.commit(
            a[9].address, 5000000000000000000,
            "0x5d46b2de62ae6807ad8a2cf1342b2b7bf547328f23a5439511297798b9cadfbc",
            {"from": a[1]}
        )