import os
from dotenv import load_dotenv
from web3 import Web3, HTTPProvider
import asyncio
from logging import getLogger, DEBUG, INFO, basicConfig
from colorama import Fore, Style
from eth_utils import remove_0x_prefix
from math import ceil
from multiprocessing import Process, Queue
from web3.middleware import geth_poa_middleware

from utils.client import Client
from utils.contract import DeployedContract
from watcher import ContractEventsWatcher
from settings import OracleConfig
from database import Database
from commit_actions import run as worker_target, commit_aсtions, WorkerConfig, WorkerResult, ACTION_ALREADY_PERFORMED

MAX_CHUNK_SIZE = 200

basicConfig(
    format=(
        f"{Style.DIM}%(asctime)s "
        f"{Style.RESET_ALL}{Style.BRIGHT}%(levelname)s "
        f"{Style.RESET_ALL}{Fore.GREEN}%(name)s{Style.DIM}/"
        f"{Style.RESET_ALL}{Style.BRIGHT}{Fore.GREEN}%(funcName)s"
        f"{Style.RESET_ALL}: %(message)s"
    )
)
logger = getLogger("oracle")
logger.setLevel(DEBUG)


class Oracle:
    def __init__(
        self,
        validators_abi,
        private_key: str,
        database: Database,
        side: str,

        source_bridge_abi,
        source_web3: Web3,
        source_url: str,
        source_bridge: str,
        source_block: int,
        source_gas_price: int,

        destination_bridge_abi,
        destination_url: str,
        destination_bridge: str,
        destination_gas_price: int,
    ):
        self.side = side
        self.private_key = private_key
        self.source_web3 = source_web3
        self.validators_abi = validators_abi
        self.source_client = Client(
            source_url, private_key, source_gas_price)
        self.destination_client = Client(
            destination_url, private_key, destination_gas_price)
        logger.debug(f"my address = {self.destination_client.account.address}")
        self.source_contract = DeployedContract(
            self.source_client,
            address=source_bridge,
            abi=source_bridge_abi,
        )
        self.destination_contract = DeployedContract(
            self.destination_client,
            address=destination_bridge,
            abi=destination_bridge_abi,
        )
        self.sequential_transactions = False

        self.database = database

        self.watcher = ContractEventsWatcher(
            source_web3,
            address=source_bridge,
            abi=source_bridge_abi,
            from_block_number=self.database.get_last_block(side) + 1,
            poll_interval=1,
        )
        self.watcher.add_event_processor(
            event_name="bridgeActionInitiated",
            handler_func=self.bridge_action_handler,
            many_at_once=True,
        )
        self.watcher.post_poll(self.post_poll)

        if self.side == "right":
            self.watcher.add_event_processor(
                event_name="commitsCollected",
                handler_func=self.commits_collected_handler,
            )

        self.post_poll_lock = asyncio.Lock()
        self.transaction_lock = asyncio.Lock()

    async def bridge_action_handler(self, events):
        last_block = events[-1]["blockNumber"]
        actions = {
            event["transactionHash"]: (event["args"]["recipient"], event["args"]["amount"]) for event in events
        }

        if logger.isEnabledFor(DEBUG):
            logger.debug(f"received these actions: {actions}")
        self.database.register_actions(self.side, actions)
        self.database.set_last_block(self.side, last_block)

    async def commits_collected_handler(self, event):
        args = event["args"]
        id, amount = args["id"], args["commits"]
        logger.debug(f"recieved a commitsCollected({id}, {amount}) event")

        signed_data_coroutines = (
            asyncio.to_thread(self.source_contract.getCommit(id, nth).call)
            for nth in range(amount)
        )
        *signed_data, (recipient, amount) = await asyncio.gather(
            *signed_data_coroutines,
            asyncio.to_thread(
                self.source_contract.getTransferDetails(id).call),
        )
        r_arguments = [r for r, s, v in signed_data]
        s_arguments = [s for r, s, v in signed_data]
        v_arguments = [v for r, s, v in signed_data]

        is_confirmed = await asyncio.to_thread(
            self.destination_contract.isIdConfirmed(id).call,
        )

        if is_confirmed:
            logger.debug(f"event {id} is already applied; returning")
            return

        while True:
            try:
                await asyncio.to_thread(
                    self.destination_contract.applyCommits(
                        recipient, amount, id,
                        r_arguments, s_arguments, v_arguments,
                    ).transact,
                    wait_for_receipt=False,
                )

                logger.debug(f"event {id} is applied")
                break
            except Exception as error:
                message = (
                    error.args[0]["message"]
                    if type(error.args[0]) == dict
                    else ""
                )

                if ACTION_ALREADY_PERFORMED in message:
                    logger.debug(f"event {id} was already applied; returning")
                    break

                if "nonce" in message:
                    logger.warning(
                        f"Detected an error due to parallel transactions: {error}; "
                        "retrying"
                    )
                    continue

                logger.error(
                    f"failed to register action {id}: {error}; retrying")
                continue

        self.database.set_last_block(self.side, event["blockNumber"])

    async def post_poll(self):
        if not self.database.has_pending_actions(self.side):
            logger.debug("no action need to be processed")
            return

        if self.post_poll_lock.locked():
            logger.debug("previous call hasn't finished yet; returning")
            return

        async with self.post_poll_lock:
            while True:
                if await self.commit_iteration():
                    logger.debug("finished without failures")
                    return

                logger.debug("finished with failures; trying again")

    async def commit_iteration(self):
        if not await self.am_i_validator():
            logger.warning("I'm not a validator; returning")
            return

        logger.debug("starting")
        transaction_nonce = await asyncio.to_thread(
            self.destination_client.w3.eth.getTransactionCount,
            self.destination_client.account.address,
        )
        actions = self.database.get_pending_actions(self.side)

        if self.sequential_transactions or len(actions) <= MAX_CHUNK_SIZE:
            worker_config = WorkerConfig(
                actions=actions,
                my_address=self.destination_client.account.address,
                side=self.side,
                private_key=self.private_key,
                first_transaction_nonce=transaction_nonce
            )
            result = await commit_aсtions(
                worker_config,
                self.source_contract,
                self.destination_contract,
                transaction_lock=(
                    self.transaction_lock
                    if self.sequential_transactions
                    else None
                )
            )
        else:
            action_ids = list(actions.keys())
            chunks_amount = ceil(len(action_ids) / MAX_CHUNK_SIZE)
            chunk_size = len(action_ids) // chunks_amount
            chunks = [
                action_ids[nth * chunk_size:(nth + 1) * chunk_size]
                for nth in range(chunks_amount)
            ]

            left_tokens = action_ids[chunk_size * chunks_amount:]
            for nth, token in enumerate(left_tokens):
                chunks[nth].append(token)

            worker_configs = []
            offset = 0
            for nth, chunk in enumerate(chunks):
                worker_configs.append(WorkerConfig(
                    action={action_id: actions[action_id]
                            for action_id in chunk},
                    my_address=self.destination_client.account.address,
                    side=self.side,
                    private_key=self.private_key,
                    first_transaction_nonce=transaction_nonce + offset
                ))
                offset += len(chunk)

            queue = Queue(chunks_amount)
            processes = [
                Process(target=worker_target, args=(config, queue))
                for config in worker_configs
            ]
            for process in processes:
                process.start()

            await asyncio.gather(*(
                asyncio.to_thread(process.join) for process in processes
            ))

            result = WorkerResult()

            for _ in range(chunks_amount):
                try:
                    worker_result = queue.get_nowait()
                except:
                    break

                result.failed_actions.update(worker_result.failed_actions)
                if worker_result.detected_nonce_race:
                    result.detected_nonce_race = True

        if result.detected_nonce_race:
            self.sequential_transactions = True

        commited_actions = set(actions.keys()).difference(
            result.failed_actions)
        self.database.drop_actions(commited_actions)

        return len(result.failed_actions) == 0

    async def am_i_validator(self):
        validators_address = await asyncio.to_thread(
            self.destination_contract.validatorSet().call
        )
        validators_contract = DeployedContract(
            self.destination_client,
            address=validators_address,
            abi=self.validators_abi,
        )
        validators = await asyncio.to_thread(
            validators_contract.getValidators().call,
        )

        return self.destination_client.account.address in validators


async def start():
    load_dotenv()
    config = OracleConfig()

    left_web3 = Web3(HTTPProvider(config.left_url))
    left_web3.middleware_onion.inject(geth_poa_middleware, layer=0)
    right_web3 = Web3(HTTPProvider(config.right_url))
    right_web3.middleware_onion.inject(geth_poa_middleware, layer=0)

    database = Database(
        path=config.database,
        left_start_block=config.left_start_block,
        right_start_block=config.right_start_block,
    )

    left_oracle = Oracle(
        validators_abi=config.validators_build["abi"],
        private_key=config.private_key,
        database=database,
        side="left",

        source_bridge_abi=config.left_bridge_build["abi"],
        source_web3=left_web3,
        source_url=config.left_url,
        source_bridge=config.left_address,
        source_block=config.left_start_block,
        source_gas_price=config.left_gas_price,

        destination_bridge_abi=config.right_bridge_build["abi"],
        destination_url=config.right_url,
        destination_bridge=config.right_address,
        destination_gas_price=config.right_gas_price,
    )
    right_oracle = Oracle(
        validators_abi=config.validators_build["abi"],
        private_key=config.private_key,
        database=database,
        side="right",

        source_bridge_abi=config.right_bridge_build["abi"],
        source_web3=right_web3,
        source_url=config.right_url,
        source_bridge=config.right_address,
        source_block=config.right_start_block,
        source_gas_price=config.right_gas_price,

        destination_bridge_abi=config.left_bridge_build["abi"],
        destination_url=config.left_url,
        destination_bridge=config.left_address,
        destination_gas_price=config.left_gas_price,
    )

    await asyncio.gather(
        left_oracle.watcher.run_polling(),
        right_oracle.watcher.run_polling(),
    )


if __name__ == '__main__':
    asyncio.run(start())
