"""
Типо конфиг

Решил использовать эту либу: https://habr.com/ru/post/485236/
Думаб пригодится
"""
import json
import os
from pathlib import Path

from betterconf import Config, field
from betterconf.config import AbstractProvider
from betterconf.caster import to_int


class BuildProvider(AbstractProvider):
    LOCAL_BUILD_PATH = '../contracts/build/contracts/'

    def __init__(self, contract: str):
        build_path = Path(
            os.getenv('BUILD_PATH', self.LOCAL_BUILD_PATH)) / f"{contract}.json"
        with open(build_path) as file:
            self.build = json.load(file)

    def get(self, name: str):
        return self.build


class OracleConfig(Config):
    private_key = field("PRIVKEY")

    left_url = field("LEFT_RPCURL")
    right_url = field("RIGHT_RPCURL")

    left_gas_price = field("LEFT_GASPRICE", caster=to_int)
    right_gas_price = field("RIGHT_GASPRICE", caster=to_int)

    left_address = field("LEFT_ADDRESS")
    right_address = field("RIGHT_ADDRESS")

    left_start_block = field("LEFT_START_BLOCK", caster=to_int)
    right_start_block = field("RIGHT_START_BLOCK", caster=to_int)

    database = field("DATABASE_PATH", default="../oracle-data/db/state.db")
    left_bridge_build = field(
        'contract_build', provider=BuildProvider("LeftBridge"))
    right_bridge_build = field(
        'contract_build', provider=BuildProvider("RightBridge"))
    validators_build = field(
        'contract_build', provider=BuildProvider("BridgeValidators"))

    sequential_transactions = False
