from eth_account import Account
from web3 import Web3, HTTPProvider


class Client:
    def __init__(self, rpc_url, private_key, gas_price):
        self.account = Account.from_key(private_key)
        self.w3 = Web3(HTTPProvider(rpc_url))
        self.gas_price = gas_price

    def transact(self, tx, wait_for_receipt=True):
        signed_tx = self.account.sign_transaction(tx).rawTransaction
        tx_id = self.w3.eth.sendRawTransaction(signed_tx)
        if wait_for_receipt:
            tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_id)
            return tx_receipt

    def balance(self):
        return self.get_balance(self.account.address)

    def get_balance(self, address):
        return self.w3.eth.getBalance(address)

    def deploy(self, abi, bytecode, *args, **kwargs):
        contract = self.w3.eth.contract(
            bytecode=bytecode,
            abi=abi,
        )
        tx_dict = {
            'from': self.account.address,
            'gasPrice': self.gas_price,
            'nonce': self.w3.eth.getTransactionCount(self.account.address)
        }
        tx_dict['gas'] = contract.constructor(
            *args, **kwargs).estimateGas(tx_dict)
        tx = contract.constructor(*args, **kwargs).buildTransaction(tx_dict)

        # address = self.transact(tx)['contractAddress']
        return self.transact(tx)
