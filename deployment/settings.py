import json
import os

from betterconf import Config, field
from betterconf.caster import AbstractCaster, to_int
from betterconf.config import AbstractProvider


class BuildProvider(AbstractProvider):
    def __init__(self, build_path):
        with open(build_path) as file:
            self.build = json.load(file)

    def get(self, name: str):
        return self.build


class ValidatorsCaster(AbstractCaster):
    def cast(self, val: str):
        return val.split()


to_validators = ValidatorsCaster()


class DeployerConfig(Config):
    private_key = field('PRIVKEY')

    left_rpc_url = field('LEFT_RPCURL')
    left_gas_price = field('LEFT_GASPRICE', default=5000000000, caster=to_int)

    right_rpc_url = field('RIGHT_RPCURL')
    right_gas_price = field('RIGHT_GASPRICE', default=5000000000, caster=to_int)

    validators = field('VALIDATORS', caster=to_validators)
    threshold = field('THRESHOLD', caster=to_int)

    bridge_validators_build = field(
        provider=BuildProvider(
            os.getenv(
                'BRIDGE_VALIDATORS_BUILD',
                '../contracts/build/contracts/BridgeValidators.json'
            )
        )
    )

    bridge_build_left = field(
        provider=BuildProvider(os.getenv('BRIDGE_BUILD', '../contracts/build/contracts/LeftBridge.json'))
    )

    bridge_build_right = field(
        provider=BuildProvider(os.getenv('BRIDGE_BUILD', '../contracts/build/contracts/RightBridge.json'))
    )

