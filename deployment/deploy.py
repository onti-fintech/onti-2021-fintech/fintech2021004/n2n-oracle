from utils.client import Client
from settings import DeployerConfig
import dotenv

dotenv.load_dotenv()


def main():
    cfg = DeployerConfig()

    left_client = Client(cfg.left_rpc_url, cfg.private_key, cfg.left_gas_price)
    right_client = Client(cfg.right_rpc_url, cfg.private_key, cfg.right_gas_price)

    receipt = left_client.deploy(
        cfg.bridge_validators_build['abi'],
        cfg.bridge_validators_build['bytecode'],
        _validators=cfg.validators,
        _threshold=cfg.threshold,
    )
    print(f'#1 [LEFT] Validators Set deployed at {receipt["contractAddress"]}')

    left_bridge_receipt = left_client.deploy(
        cfg.bridge_build_left['abi'],
        cfg.bridge_build_left['bytecode'],
        _validatorSet=receipt['contractAddress'],
    )
    print(f'#2 [LEFT] Bridge deployed at {left_bridge_receipt["contractAddress"]}')

    receipt = right_client.deploy(
        cfg.bridge_validators_build['abi'],
        cfg.bridge_validators_build['bytecode'],
        _validators=cfg.validators,
        _threshold=cfg.threshold
    )
    print(f'#3 [RIGHT] Validators Set deployed at {receipt["contractAddress"]}')

    right_bridge_receipt = right_client.deploy(
        cfg.bridge_build_right['abi'],
        cfg.bridge_build_right['bytecode'],
        _validatorSet=receipt['contractAddress'],
    )
    print(f'#4 [RIGHT] Bridge deployed at {right_bridge_receipt["contractAddress"]}')

    print(f'#5 [LEFT] Bridge deployed at block {left_bridge_receipt["blockNumber"]}')
    print(f'#6 [RIGHT] Bridge deployed at block {right_bridge_receipt["blockNumber"]}')


if __name__ == '__main__':
    main()
